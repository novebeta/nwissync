import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.sql.*;
public class StartGui extends JFrame {
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/";

    //  Database credentials
    static final String USER = "username";
    static final String PASS = "password";
    TrayIcon trayIcon;
    SystemTray tray;
    private JPanel MainPanel;
    private JScrollPane mainScroll;
    private JMenuBar menubar;
    private JTextArea txtNote;
    private int cnt;
    private final String USER_AGENT = "Mozilla/5.0";
    private Task task;
    private Timer timer;
    class Task extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
            Process p;
            try {
                p = Runtime.getRuntime().exec("php yiic sync");
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(p.getInputStream()));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    addNote(line);
                }
                p.waitFor();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        /*
         * Executed in event dispatching thread
         */
        @Override
        public void done() {
            Toolkit.getDefaultToolkit().beep();
        }
    }
    public StartGui() {
        setTitle("NWIS Sync");
        menubar = new JMenuBar();
        JMenu file = new JMenu("File");
//        file.setMnemonic(KeyEvent.VK_F);
        JMenuItem fileExit = new JMenuItem("Exit");
//        fileExit.setMnemonic(KeyEvent.VK_C);
        fileExit.setToolTipText("Exit application");
//        fileExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W,
//                ActionEvent.CTRL_MASK));

        fileExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });
        file.add(fileExit);
        menubar.add(file);
        setJMenuBar(menubar);
        setContentPane(MainPanel);
        ActionListener actListner = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    task = new Task();
                    task.execute();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        try {
            if (SystemTray.isSupported()) {
                System.out.println("system tray supported");
                tray = SystemTray.getSystemTray();
                Image image = Toolkit.getDefaultToolkit().getImage("../images/icon-natasha.gif");
                ActionListener exitListener = new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Exiting....");
                        System.exit(0);
                    }
                };
                PopupMenu popup = new PopupMenu();
                MenuItem defaultItem = new MenuItem("Exit");
                defaultItem.addActionListener(exitListener);
                popup.add(defaultItem);
                defaultItem = new MenuItem("Open");
                defaultItem.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        tray.remove(trayIcon);
                        setVisible(true);
                        setExtendedState(JFrame.NORMAL);
                    }
                });
                popup.add(defaultItem);
                trayIcon = new TrayIcon(image, "NWIS Sync", popup);
                trayIcon.setImageAutoSize(true);
                addWindowStateListener(new WindowStateListener() {
                    public void windowStateChanged(WindowEvent e) {
                        if (e.getNewState() == ICONIFIED) {
                            try {
                                tray.add(trayIcon);
                                setVisible(false);
                                System.out.println("added to SystemTray");
                            } catch (AWTException ex) {
                                System.out.println("unable to add to tray");
                            }
                        }
                        if (e.getNewState() == 7) {
                            try {
                                tray.add(trayIcon);
                                setVisible(false);
                                System.out.println("added to SystemTray");
                            } catch (AWTException ex) {
                                System.out.println("unable to add to system tray");
                            }
                        }
                        if (e.getNewState() == MAXIMIZED_BOTH) {
                            tray.remove(trayIcon);
                            setVisible(true);
                            System.out.println("Tray icon removed");
                        }
                        if (e.getNewState() == NORMAL) {
                            tray.remove(trayIcon);
                            setVisible(true);
                            System.out.println("Tray icon removed");
                        }
                    }
                });
//                setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("../images/icon-natasha.gif")));
                tray.add(trayIcon);
            } else {
                System.out.println("system tray not supported");
            }
            timer = new Timer(1200 * 1000, actListner);
            timer.start();
        } catch (Exception ex) {
        }
    }
    public static void main(String[] args) {
        JFrame frame = new StartGui();
//        frame.setContentPane(new StartGui().MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:nwissync.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");
//        frame.setVisible(true);
    }
    private void executeCommand(String command) {
//        StringBuffer output = new StringBuffer();
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                addNote(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void addNote(String note) {
        txtNote.append(note + "\n");
    }
    private void sendGet(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        //print result
        addNote(response.toString());
    }
    // HTTP POST request
    private void sendPost(String url) throws Exception {
//        String url = "https://selfsolve.apple.com/wcResults.do";
        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        //print result
        addNote(response.toString());
    }
}
